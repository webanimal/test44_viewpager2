package ru.webanimal.test44_viewpager2

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.PagerSnapHelper
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.fragment_history.*
import kotlinx.android.synthetic.main.layout_history_list_bottom.*
import kotlinx.android.synthetic.main.layout_history_list_selected_bottom.*
import timber.log.Timber

class HistoryFragment: Fragment(R.layout.fragment_history), View.OnClickListener, OnBackPressedResolver {

    private val historySelectedSnapHelper = PagerSnapHelper()
    private var bottomSheetBehavior: BottomSheetBehavior<View>? = null

    private var history = listOf<List<String>>()
    private var dates = listOf<String>()
    private var pageChangedOnResume: Boolean = false

    private val pageChangeCallback = object: ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            super.onPageSelected(position)

            if (pageChangedOnResume) {
                pageChangedOnResume = false
                return
            }

            updateDataSet()
            setData(history, position)
        }
    }

    private val onDataSetClickListener = object: OnDataSetClickListener {
        override fun onClick(dataSet: List<String>) {
            if (isResumed) {
                (rvSelectedHistory?.adapter as? HistorySelectedAdapter)?.let {
                    it.updateData(dataSet)
                    it.notifyDataSetChanged()
                    updateBottomViewsVisibility(showSelectedHistory = true)
                }
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        updateBottomViewsVisibility(showSelectedHistory = false)
        // This is to manipulate with collapsing state etc.
        bottomSheetBehavior = BottomSheetBehavior.from(llBottomHistory)

        vpHistory.offscreenPageLimit = 3
        vpHistory.adapter = HistoryListAdapter(onDataSetClickListener)
        vpHistory.registerOnPageChangeCallback(pageChangeCallback)

        rvSelectedHistory.adapter = HistorySelectedAdapter()
        historySelectedSnapHelper.attachToRecyclerView(rvSelectedHistory)

        ivPrev.setOnClickListener(this)
        ivNext.setOnClickListener(this)
    }

    override fun onResume() {
        super.onResume()
        initData()
        val lastItemPosition = history.size - 1
        setData(history, lastItemPosition)
        pageChangedOnResume = true
        vpHistory.setCurrentItem(lastItemPosition, false)
    }

    override fun onDestroyView() {
        vpHistory?.unregisterOnPageChangeCallback(pageChangeCallback)
        super.onDestroyView()
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.ivPrev -> onPrevClick()
            R.id.ivNext -> onNextClick()
        }
    }

    override fun onBackPressed(): Boolean {
        return if (shouldCollapseApp()) {
            true
        } else {
            updateBottomViewsVisibility(showSelectedHistory = false)
            false
        }
    }

    private fun onPrevClick() {
        val currentItem = vpHistory.currentItem
        if (isResumed && currentItem > 0) {
            vpHistory.setCurrentItem(currentItem - 1, true)
        }
    }

    private fun onNextClick() {
        val currentItem = vpHistory.currentItem
        if (isResumed && currentItem < history.size) {
            vpHistory.setCurrentItem(currentItem + 1, true)
        }
    }

    private fun setData(dataSet: List<List<String>>, position: Int) {
        tvDate.text = dates[position]
        (vpHistory.adapter as? HistoryListAdapter)?.updateData(dataSet)
    }

    private fun updateDataSet() {
        // Doing something
        history.forEach { Timber.d("TEST::updateDataSet day list size:${it.size}") }
    }

    private fun initData() {
        history = HistoryDataSource.getHistory()
        dates = HistoryDataSource.getDates()
    }

    private fun updateBottomViewsVisibility(showSelectedHistory: Boolean) {
        if (showSelectedHistory) {
            llBottomHistory.visibility = View.INVISIBLE
            llBottomSelectedHistory.visibility = View.VISIBLE

        } else {
            llBottomHistory.visibility = View.VISIBLE
            llBottomSelectedHistory.visibility = View.INVISIBLE
        }
    }

    private fun shouldCollapseApp(): Boolean {
        return llBottomHistory.visibility == View.VISIBLE
    }
}