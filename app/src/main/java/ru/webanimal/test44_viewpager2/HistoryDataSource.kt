package ru.webanimal.test44_viewpager2

class HistoryDataSource {

    companion object {
        private const val DATE_01 = "21.06.2020"
        private const val DATE_02 = "22.06.2020"
        private const val DATE_03 = "23.06.2020"
        private const val DATE_04 = "24.06.2020"
        private const val DATE_05 = "25.06.2020"
        private const val DATE_06 = "26.06.2020"
        private const val DATE_07 = "27.06.2020"
        private const val DATE_08 = "28.06.2020"
        private const val DATE_09 = "29.06.2020"
        private const val DATE_10 = "30.06.2020"
        private const val DATE_11 = "01.07.2020"

        private val history = arrayOf(
            listOf(
                DATE_01,
                DATE_01,
                DATE_01,
                DATE_01,
                DATE_01,
                DATE_01,
                DATE_01,
                DATE_01,
                DATE_01,
                DATE_01,
                DATE_01,
                DATE_01,
                DATE_01
            ),
            listOf(DATE_02, DATE_02, DATE_02, DATE_02, DATE_02, DATE_02, DATE_02, DATE_02),
            listOf(),
            listOf(
                DATE_04,
                DATE_04,
                DATE_04,
                DATE_04,
                DATE_04,
                DATE_04,
                DATE_04,
                DATE_04,
                DATE_04,
                DATE_04
            ),
            listOf(
                DATE_05,
                DATE_05,
                DATE_05,
                DATE_05,
                DATE_05,
                DATE_05,
                DATE_05,
                DATE_05,
                DATE_05,
                DATE_05,
                DATE_05,
                DATE_05,
                DATE_05,
                DATE_05
            ),
            listOf(),
            listOf(
                DATE_07,
                DATE_07,
                DATE_07,
                DATE_07,
                DATE_07,
                DATE_07,
                DATE_07,
                DATE_07,
                DATE_07,
                DATE_07,
                DATE_07,
                DATE_07,
                DATE_07,
                DATE_07
            ),
            listOf(DATE_08, DATE_08, DATE_08, DATE_08),
            listOf(
                DATE_09,
                DATE_09,
                DATE_09,
                DATE_09,
                DATE_09,
                DATE_09,
                DATE_09,
                DATE_09,
                DATE_09,
                DATE_09,
                DATE_09,
                DATE_09,
                DATE_09,
                DATE_09,
                DATE_09,
                DATE_09,
                DATE_09
            ),
            listOf(DATE_10),
            listOf(DATE_11, DATE_11, DATE_11, DATE_11, DATE_11, DATE_11)
        )

        private val dates = arrayOf(
            DATE_01,
            DATE_02,
            DATE_03,
            DATE_04,
            DATE_05,
            DATE_06,
            DATE_07,
            DATE_08,
            DATE_09,
            DATE_10,
            DATE_11
        )

        fun getHistory(): List<List<String>> {
            return historyWithNumericPrefix()
        }

        fun getDates(): List<String> {
            return dates.toList()
        }

        private fun historyWithNumericPrefix(): List<List<String>> {
            var i: Int

            var newHistory = arrayListOf<List<String>>()
            for (timeline in history) {
                i = 0
                var newTimeline = arrayListOf<String>()
                for (track in timeline) {
                    i++
                    newTimeline.add("$i.  $track")
                }
                newHistory.add(newTimeline)
            }
            return newHistory
        }
    }
}