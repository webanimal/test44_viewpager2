package ru.webanimal.test44_viewpager2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment

class MainActivity : AppCompatActivity() {

    private var currentFragment: Fragment? = null
    private val historyFragmentTag: String = "HistoryFragmentTag"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ensureFragmentContainerNotEmpty()
    }

    private fun ensureFragmentContainerNotEmpty() {
        if (supportFragmentManager.isStateSaved) return
        supportFragmentManager.executePendingTransactions()
        currentFragment = supportFragmentManager.findFragmentByTag(historyFragmentTag)
        if (currentFragment == null) {
            val fragment = HistoryFragment()
            currentFragment = fragment
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, fragment, historyFragmentTag)
                .commit()
        }
    }

    override fun onBackPressed() {
        var shouldCollapseApp = true
        if (currentFragment != null) {
            currentFragment?.let {
                if (it is OnBackPressedResolver) {
                    shouldCollapseApp = it.onBackPressed()
                }
            }
        }

        if (shouldCollapseApp) {
            super.onBackPressed()
        }
    }
}
