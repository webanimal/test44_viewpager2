package ru.webanimal.test44_viewpager2

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class HistoryListAdapter(
    private val listener: OnDataSetClickListener?
): RecyclerView.Adapter<HistoryListViewHolder>() {

    private var dataSet = arrayListOf<List<String>>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryListViewHolder {
        return HistoryListViewHolder.create(parent, listener)
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    override fun onBindViewHolder(holder: HistoryListViewHolder, position: Int) {
        holder.bind(dataSet[position])
    }

    fun updateData(newData: List<List<String>>) {
        dataSet.clear()
        dataSet.addAll(newData)
    }
}