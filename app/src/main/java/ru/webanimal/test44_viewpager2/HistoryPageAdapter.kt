package ru.webanimal.test44_viewpager2

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

class HistoryPageAdapter(
    private val pagerChildClickListener: PagerChildClickListener
): RecyclerView.Adapter<HistoryPageViewHolder>() {

    private var dataSet = arrayListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryPageViewHolder {
        return HistoryPageViewHolder.create(parent, pagerChildClickListener)
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    override fun onBindViewHolder(holder: HistoryPageViewHolder, position: Int) {
        holder.bind(dataSet[position])
    }

    fun updateData(newData: List<String>, diffCallback: HistoryPageDiffCallback) {
        diffCallback.updateItems(dataSet, newData)

        dataSet.clear()
        dataSet.addAll(newData)

        val result = DiffUtil.calculateDiff(diffCallback)
        result.dispatchUpdatesTo(this)
    }
}