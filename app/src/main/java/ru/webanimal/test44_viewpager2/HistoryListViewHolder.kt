package ru.webanimal.test44_viewpager2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class HistoryListViewHolder(
    itemView: View,
    private val listener: OnDataSetClickListener?
): ColorfulBaseViewHolder(itemView) {

    companion object {
        fun create(parent: ViewGroup, listener: OnDataSetClickListener?): HistoryListViewHolder {
            return HistoryListViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.layout_history_list_page,
                    parent,
                    false
                ),
                listener
            )
        }
    }

    private val pagerChildClickListener = object: PagerChildClickListener {
        override fun clicked() {
            listener?.onClick(dataSet)
        }
    }

    private val pageRoot: View? = itemView.findViewById(R.id.pageRoot)
    private val rvHistoryList: RecyclerView? = itemView.findViewById(R.id.rvHistoryList)
    private val adapter: HistoryPageAdapter = HistoryPageAdapter(pagerChildClickListener)
    private val diffCallback = HistoryPageDiffCallback()

    private var dataSet = listOf<String>()

    init {
        rvHistoryList?.adapter = adapter
    }

    fun bind(dataSet: List<String>) {
        this.dataSet = dataSet
        pageRoot?.setBackgroundResource(getRandomBackground())
        rvHistoryList?.let {
            adapter.updateData(dataSet, diffCallback)
        }
    }
}