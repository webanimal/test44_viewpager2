package ru.webanimal.test44_viewpager2

import androidx.recyclerview.widget.DiffUtil

class HistoryPageDiffCallback: DiffUtil.Callback() {

    private var oldList = arrayListOf<String>()
    private var newList = arrayListOf<String>()

    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return areTheSame(oldItemPosition, newItemPosition)
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return areTheSame(oldItemPosition, newItemPosition)
    }

    fun updateItems(oldItems: List<String>, newItems: List<String>) {
        oldList.clear()
        oldList.addAll(oldItems)

        newList.clear()
        newList.addAll(newItems)
    }

    private fun areTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        if (oldList.isEmpty() || newList.isEmpty()) {
            return false
        }

        val oldSize = oldList.size
        val newSize = newList.size

        return if (oldItemPosition >= oldSize || newItemPosition >= newSize) {
            false
        } else {
            oldList[oldItemPosition] == newList[newItemPosition]
        }
    }
}