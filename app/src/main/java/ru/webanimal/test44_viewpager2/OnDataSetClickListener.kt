package ru.webanimal.test44_viewpager2

interface OnDataSetClickListener {
    fun onClick(dataSet: List<String>)
}