package ru.webanimal.test44_viewpager2

interface OnBackPressedResolver {
    fun onBackPressed(): Boolean
}