package ru.webanimal.test44_viewpager2

interface PagerChildClickListener {
    fun clicked()
}