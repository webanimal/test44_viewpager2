package ru.webanimal.test44_viewpager2

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlin.random.Random

open class ColorfulBaseViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {

    private val colors = intArrayOf(
        R.color.colorSoftBlue,
        R.color.colorLightBlue,
        R.color.colorLightGreen,
        R.color.colorGreen,
        R.color.colorSoftGreen,
        R.color.colorWarmYellow,
        R.color.colorSoftOrange,
        R.color.colorPink,
        R.color.colorSoftPink,
        R.color.colorSoftViolet
    )

    protected fun getRandomBackground(): Int {
        return colors[Random.nextInt(colors.size)]
    }
}