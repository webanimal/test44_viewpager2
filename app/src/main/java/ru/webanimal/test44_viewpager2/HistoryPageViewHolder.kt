package ru.webanimal.test44_viewpager2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class HistoryPageViewHolder(
    itemView: View,
    private val pagerChildClickListener: PagerChildClickListener
): ColorfulBaseViewHolder(itemView) {

    companion object {
        fun create(parent: ViewGroup, pagerChildClickListener: PagerChildClickListener): HistoryPageViewHolder {
            return HistoryPageViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.layout_history_list_page_item,
                    parent,
                    false
                ),
                pagerChildClickListener
            )
        }
    }

    private val tvPageTitle: TextView? = itemView.findViewById(R.id.tvPageTitle)

    fun bind(data: String) {
        tvPageTitle?.setOnClickListener {
            pagerChildClickListener?.clicked()
        }
        tvPageTitle?.text = data
    }
}