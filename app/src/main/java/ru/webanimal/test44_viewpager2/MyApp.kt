package ru.webanimal.test44_viewpager2

import androidx.multidex.MultiDexApplication
import timber.log.Timber

class MyApp: MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())
    }
}