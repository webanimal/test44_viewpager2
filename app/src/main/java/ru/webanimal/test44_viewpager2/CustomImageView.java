package ru.webanimal.test44_viewpager2;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.TypedValue;

import androidx.appcompat.widget.AppCompatImageView;

@SuppressWarnings("unused")
public class CustomImageView extends AppCompatImageView {
    private ColorStateList mTint;

    public CustomImageView(Context context) {
        super(context);
    }

    public CustomImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }

    public CustomImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        if (mTint != null && mTint.isStateful()) {
            updateTintColor();
        }
    }

    public void setColorFilter(ColorStateList tint) {
        mTint = tint;
        super.setColorFilter(tint.getColorForState(getDrawableState(), 0));
        updateTintColor();
    }

    public void setColor(int color) {
        mTint = ColorStateList.valueOf(color);
        super.setColorFilter(mTint.getColorForState(getDrawableState(), 0));
        updateTintColor();
    }

    private void updateTintColor() {
        int color = mTint.getColorForState(getDrawableState(), 0);
        setColorFilter(color);
    }

    private void init(Context context, AttributeSet attrs, int defStyle) {
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CustomImageView, defStyle, 0);
        TypedValue value = a.peekValue(R.styleable.CustomImageView_tintColor);
        if (value != null) {
            if (value.type == TypedValue.TYPE_STRING) {
                mTint = a.getColorStateList(R.styleable.CustomImageView_tintColor);
            } else {
                int color = a.getColor(R.styleable.CustomImageView_tintColor, 0);
                int[][] states = new int[][]{
                        new int[]{android.R.attr.state_enabled}, // enabled
                        new int[]{-android.R.attr.state_enabled}, // disabled
                        new int[]{-android.R.attr.state_checked}, // unchecked
                        new int[]{android.R.attr.state_pressed}  // pressed
                };
                int[] colors = new int[]{color, color, color, color};
                mTint = new ColorStateList(states, colors);
            }
        }
        a.recycle();
    }
}
