package ru.webanimal.test44_viewpager2

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class HistorySelectedAdapter: RecyclerView.Adapter<HistorySelectedViewHolder>() {

    private var dataSet = arrayListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistorySelectedViewHolder {
        return HistorySelectedViewHolder.create(parent)
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    override fun onBindViewHolder(holder: HistorySelectedViewHolder, position: Int) {
        holder.bind(dataSet[position])
    }

    fun updateData(newData: List<String>) {
        dataSet.clear()
        dataSet.addAll(newData)
    }
}