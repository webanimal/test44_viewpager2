package ru.webanimal.test44_viewpager2

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

class HistorySelectedViewHolder(itemView: View): ColorfulBaseViewHolder(itemView) {

    companion object {
        fun create(parent: ViewGroup): HistorySelectedViewHolder {
            return HistorySelectedViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.layout_history_list_selected_item,
                    parent,
                    false
                )
            )
        }
    }

    private val tvPageTitle: TextView? = itemView.findViewById(R.id.tvPageTitle)

    fun bind(data: String) {
        tvPageTitle?.setBackgroundResource(getRandomBackground())
        tvPageTitle?.text = data
    }
}